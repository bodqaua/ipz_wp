const task1element = document.getElementById('task-1-result');
const task2element = document.getElementById('task-2-result');
const task3element = document.getElementById('task-3-result');
const task4element = document.getElementById('task-4-result');

const logins = ['ivan', 'ssss', 'gibs'];
const passwords = ['333', '666', '0000'];

const credentials = {
    login: '',
    password: ''
}

function checkHours(event) {
    const time = parseInt(event.target.value);
    task1element.innerText = time > 17 ? 'Добрый вечер' : 'Добрый день';
}

function checkName(event, requiredName) {
    const name = event.target.value
    task2element.innerText =  name === requiredName ? `Привет, ${name}` : 'Я вас не знаю';
}

function setCredentials(event, field) {
    credentials[field] = event.target.value
    console.log(credentials);
    checkAuth();
}

function checkAuth() {
    if (logins.includes(credentials.login) && passwords.includes(credentials.password)) {
        task3element.innerText = `Добро пожаловать, ${credentials.login}`;
    } else {
        task3element.innerText = 'Пользователь не найден';
    }
}

function findMax(event) {
    let numbers = event.target.value.split(' ');
    const someIsNaN = numbers.some(number => {
        console.log(isNaN(parseInt(number)))
        return isNaN(parseInt(number));
    })
    if (numbers.length !== 3) {
        task4element.innerText = 'Incorrect numbers count. Must be 3';
        return;
    }
    if (someIsNaN) {
        task4element.innerText = 'Invalid numbers';
        return;
    }

    numbers = numbers.map(number => parseInt(number));

    numbers.sort((a, b) => b - a);
    task4element.innerText = `Max number is ${numbers[0]}`;
}
