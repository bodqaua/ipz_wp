class BrickGame {
    ctx;
    canvas;
    ball;
    paddle;
    brickInfo;
    score = 0;
    brickRowCount = 8;
    brickColumnCount = 5;
    bricks = [];

    isDie = false;

    constructor(ctx, canvas) {
        this.ctx = ctx;
        this.canvas = canvas;
        this.init();
    }

    init() {
        this.initBall();
        this.initBrickData();
        this.initPaddle();
        this.resetScore();
        this.initBricks();
        this.initKeys();
    }

    initBrickData() {
        this.brickInfo = {
            w: 70,
            h: 20,
            padding: 10,
            offsetX: 45,
            offsetY: 60,
            visible: true
        };
    }

    resetScore() {
        this.score = 0;
    }

    setDie(timeout) {
        this.isDie = true;
        setTimeout(() => {
            this.isDie = false;
        }, timeout)
    }

    resetBallSpeed() {
        this.ball.speed = 4;
    }

    initPaddle() {
        this.paddle = {
            x: this.canvas.width / 2 - 40,
            y: this.canvas.height - 40,
            w: 80,
            h: 10,
            speed: 8,
            dx: 0
        };
    }

    initBall() {
        this.ball = {
            x: this.canvas.width / 2,
            y: this.canvas.height / 2,
            size: 10,
            speed: 4,
            dx: 4,
            dy: -4
        };
    }

    initBricks() {
        for (let i = 0; i < this.brickRowCount; i++) {
            this.bricks[i] = [];
            for (let j = 0; j < this.brickColumnCount; j++) {
                if (i % 2 === 0 && j % 2 === 1) {continue}
                if (i % 2 === 1 && j % 2 === 0) {continue}
                const x = i * (this.brickInfo.w + this.brickInfo.padding) + this.brickInfo.offsetX;
                const y = j * (this.brickInfo.h + this.brickInfo.padding) + this.brickInfo.offsetY;
                this.bricks[i][j] = {x, y, ...this.brickInfo};
            }
        }
    }

    keyDown(e) {
        if (e.key === "Right" || e.key === "ArrowRight") {
            this.paddle.dx = this.paddle.speed;
        } else if (e.key === "Left" || e.key === "ArrowLeft") {
            this.paddle.dx = -this.paddle.speed;
        }
    }

    keyUp(e) {
        if (e.key === "Right" || e.key === "ArrowRight" || e.key === "Left" || e.key === "ArrowLeft") {
            this.paddle.dx = 0;
        }
    }

    initKeys() {
        document.addEventListener("keydown", this.keyDown.bind(this));
        document.addEventListener("keyup", this.keyUp.bind(this));
    }

    drawScore() {
        this.ctx.font = "20px Arial";
        this.ctx.fillText(`Score: ${this.score}`, this.canvas.width - 100, 30);
    }

    draw() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.drawScore();
        this.drawBall();
        this.drawPaddle();
        this.drawBricks();
    }

    drawBall() {
        this.ctx.beginPath();
        this.ctx.arc(this.ball.x, this.ball.y, this.ball.size, 0, Math.PI * 2);
        this.ctx.fillStyle = this.isDie ? '#222222' : '#0095dd';
        this.ctx.fill();
        this.ctx.closePath();
    }

    drawPaddle() {
        this.ctx.beginPath();
        this.ctx.rect(this.paddle.x, this.paddle.y, this.paddle.w, this.paddle.h);
        this.ctx.fillStyle = this.isDie ? '#222222' : '#0095dd';
        this.ctx.fill();
        this.ctx.closePath();
    }

    drawBricks() {
        this.bricks.forEach(column => {
            column.forEach(brick => {
                this.ctx.beginPath();
                this.ctx.rect(brick.x, brick.y, brick.w, brick.h);
                this.ctx.fillStyle = brick.visible ? this.isDie ? '#222222' : '#0095dd' : 'transparent';
                this.ctx.fill();
                this.ctx.closePath();
            });
        });
    }

    moveBall() {
        this.ball.x += this.ball.dx;
        this.ball.y += this.ball.dy;

        if (this.ball.x + this.ball.size > canvas.width || this.ball.x - this.ball.size < 0) {
            this.ball.dx *= -1;
        }

        if (this.ball.y + this.ball.size > canvas.height || this.ball.y - this.ball.size < 0) {
            this.ball.dy *= -1;
        }

        if (
            this.ball.x - this.ball.size > this.paddle.x &&
            this.ball.x + this.ball.size < this.paddle.x + this.paddle.w &&
            this.ball.y + this.ball.size > this.paddle.y
        ) {
            this.ball.dy = -this.ball.speed;
        }

        this.bricks.forEach(column => {
            column.forEach(brick => {
                if (brick.visible) {
                    if (
                        this.ball.x - this.ball.size > brick.x &&
                        this.ball.x + this.ball.size < brick.x + brick.w &&
                        this.ball.y + this.ball.size > brick.y &&
                        this.ball.y - this.ball.size < brick.y + brick.h
                    ) {
                        this.ball.dy *= -1;
                        brick.visible = false;

                        this.increaseScore();
                    }
                }
            });
        });
        if (this.ball.y + this.ball.size > canvas.height) {
            this.showAllBricks();
            this.resetScore();
            this.resetBallSpeed();
            this.setDie(1000);
        }
    }

    increaseScore() {
        this.score++;

        if (this.score % 10 === 0) {
            this.ball.speed++;
        }

        if (this.score === 25) {
            alert('Ипз лучшие');
        }

        if (this.score % (this.brickRowCount * this.brickRowCount) === 0) {
            this.showAllBricks();
        }
    }

    showAllBricks() {
        this.bricks.forEach(column => {
            column.forEach(brick => (brick.visible = true));
        });
    }

    movePaddle() {
        this.paddle.x += this.paddle.dx;

        if (this.paddle.x + this.paddle.w > canvas.width) {
            this.paddle.x = canvas.width - this.paddle.w;
        }

        if (this.paddle.x < 0) {
            this.paddle.x = 0;
        }
    }

    start() {
        this.update();
    }

    update() {
        this.moveBall();
        this.movePaddle();
        this.draw();
        requestAnimationFrame(this.update.bind(this));
    }
}
