const randomLocation = Math.floor(Math.random() * 5);
let location1 = randomLocation;
let location2 = location1 + 1;
let location3 = location1 + 2;
let guess
let hits = 0;
let guesses = 0;
let isSunk = false;

while(isSunk === false) {
    guess = parseInt(prompt('Ready, aim, fire! (enter a number from 0-6):'), 0);
    if (guess === null) {
        break;
    }
    if (guess < 0 || guess > 6) {
        alert ('Please enter a valid cell number!');
    } else {
        guesses += 1;

        if (guess === location1 || guess === location2 || guess === location3) {
            alert('HIT');
            hits += 1;
            if (hits === 3) {
                isSunk = true;
                alert('You sank my battleship!');
                alert(`You took ${guesses} guesses to sink battleship, which means your shooting accuracy was ${3/guesses}`);
            }
        }
        else {
            alert('Miss');
        }
    }
}

//Список улучшений
// TODO
// Исправить == на ===
// let location1 = randomLocation;let location2 = location1 + 1;let location3 = location1 + 2;
// Код выше некорректен если randomLocation >=5, корабль выйдет за поля, нужна проверка
// Добавить код остановки while, т.к сложно закрыть страницу
